// const httpStatus = require('http-status');
const pick = require('../utils/pick');
// const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const logger = require('../config/logger');
const { dataService } = require('../services');

const getData = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'role']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  logger.info(`data controller > getData > filter :\n${JSON.stringify(filter, null, 2)} `);
  logger.info(`data controller > getData > options :\n${JSON.stringify(options, null, 2)} `);
  // console.log(`data controller > getData > req.app.get('config') : \n`, req.app.get('config'));

  const result = await dataService.buildResp(req, {});
  res.send(result);
});

const getItemById = catchAsync(async (req, res) => {
  logger.info(`data controller > getItemById > req.params : ${JSON.stringify(req.params)}`);

  const options = {
    searchId: parseInt(req.params.id),
  };

  // if (!items) {
  //   throw new ApiError(httpStatus.NOT_FOUND, 'Item not found');
  // }
  const result = await dataService.buildResp(req, options);
  res.send(result);
});

const getItem = catchAsync(async (req, res) => {
  logger.info(`data controller > getItem > req.params : ${JSON.stringify(req.params)}`);

  const filters = [
    {
      field: 'id',
      value: req.params.id,
    },
  ];
  const options = {
    filters: [...filters],
  };

  // if (!items) {
  //   throw new ApiError(httpStatus.NOT_FOUND, 'Item not found');
  // }
  const result = await dataService.buildResp(req, options);
  res.send(result);
});

module.exports = {
  getData,
  getItemById,
  getItem,
};
