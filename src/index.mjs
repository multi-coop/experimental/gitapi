// const mongoose = require('mongoose');
import app from './app.js';
import config from './config/config.js';
import logger from './config/logger.js';
import { readYamlFile, readSourceFiles } from './utils/sourceData.js';
import { DataFrame as Dframe } from 'data-forge';

// read config file yaml
const yamlFile = config.files || 'config-gitapi.yaml';
logger.info(`index.js > yamlFile : ${yamlFile}`);
const configData = await readYamlFile(yamlFile);
// console.log('index.js > configData :\n', configData);

// SET GLOBAL DATAFRAMES
const dataSources = configData.files;
// console.log('index.js > dataSources :\n', dataSources);

// RETRIEVE DATA CSV
const sources = await readSourceFiles(dataSources);
console.log('\nindex.js > sources :\n', sources);
const data = sources[0].data;
const headers = sources[0].headers;
const schema = sources[0].schema;
const fields = schema.fields;
console.log('index.js > headers :\n', headers);
console.log('index.js > schema :\n', schema);

// data-forge > dataframe
let df = new Dframe(data);
console.log('\nindex.js > df.head(3).toString() :\n', df.head(3).toString());
console.log('... \n');

app.set('config', configData);

// data-forge > dataset schema
let dfSchema = df.getColumns().toArray().map(col => {
  // console.log('\nindex.js > col.name :', col.name);
  const label = headers[col.name].trim();
  // console.log('index.js > label :', label);
  const field = fields.find(f => f.name === label);
  // console.log('index.js > field :\n', field);
  return {
    index: parseInt(col.name),
    type: field ? field.type : 'string',
    autoType: col.type,
    name: label,
  }
});
dfSchema = dfSchema.map(f => {
  if (f.type != f.autoType) {
    // do something on df
    const idx = f.index.toString()
    let colSerie = df.getSeries(idx);
    // convert type
    colSerie = colSerie.select(value => {
      let val = value;
      switch(f.type) {
        case 'number':
          val = Number(val)
          break;
        case 'integer':
          val = parseInt(val)
          break;
        case 'boolean':
          valLow = val.trim().toLowerCase();
          if (valLow === 'true' || valLow === 'false') {
            val = (valLow === 'true')
          } else {
            val = false
          }
          break;
      } 
      return val
    })
    df = df.withSeries(idx, colSerie)
  }
  delete f.autoType;
  delete f.index;
  return { ...f }
})
console.log('index.js > dfSchema :\n', dfSchema);
console.log('... \n');

app.set('df', df);
app.set('dfSchema', dfSchema);
app.set('headers', headers);

const server = app.listen(config.port, () => {
  logger.info(`Listening to port : ${config.port}`);
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
