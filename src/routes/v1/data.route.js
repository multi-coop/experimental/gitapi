const express = require('express');
// const auth = require('../../middlewares/auth');
// const validate = require('../../middlewares/validate');
// const userValidation = require('../../validations/user.validation');
// const userController = require('../../controllers/user.controller');
const dataController = require('../../controllers/data.controller');

const router = express.Router();

router
  .route('/')
  .get(dataController.getData);

router
  .route('/:id')
  .get(dataController.getItemById);
  // .get(auth('getUsers'), validate(userValidation.getUser), userController.getUser);

module.exports = router;

/**
 * @swagger
 * /data:
 *
 *   get:
 *     summary: Get dataset
 *     description: Get facets from dataset.
 *     tags: [Data]
 *     parameters:
 *       - $ref: '#/components/parameters/SortBy'
 *       - $ref: '#/components/parameters/Limit'
 *       - $ref: '#/components/parameters/Page'
 *       - $ref: '#/components/parameters/ShowModel'
 *       - $ref: '#/components/parameters/Format'
 *     responses:
 *       "200":
 *         $ref: '#/components/responses/Dataset'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /data/{id}:
 *   get:
 *     summary: Get an item from dataset
 *     description: Get an item from dataset
 *     tags: [Data]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: integer
 *           default: 1
 *         description: item id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/DataItem'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *

 */
