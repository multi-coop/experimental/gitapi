/* eslint-disable no-console */
const httpStatus = require('http-status');
// const logger = require('./config/logger');
const ApiError = require('../utils/ApiError');

const len = async (df) => {
  // console.log('data.service > dataframes > len > df.toString() :', df.toString());
  const result = df.length;
  return result;
};

// TO DO ...
const search = async (df, queries = []) => {
  // console.log('data.service > dataframes > search > df.toString() :', df.toString());
  console.log('data.service > dataframes > search > queries :', queries);
  const result = df;
  // queries && queries.forEach((q) => {
  //   result = result.filter(result.get(q.field).eq(q.value));
  // });
  return result;
};

const filter = async (df, filters = []) => {
  console.log('data.service > dataframes > filter > df.head(3).toString() :\n', df.head(3).toString());
  console.log('data.service > dataframes > filter > filters :', filters);
  let results = df;
  // eslint-disable-next-line no-unused-expressions
  filters && filters.forEach((f) => {
      // console.log('data.service > dataframes > filter > results.toString() :', results.toString());
      results = results.where((row) => row[f.field] === f.value); // data-forge
    });
  if (!results.count()) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Data not found');
  }
  console.log('data.service > dataframes > filter > results.head(3).toString() :\n', results.head(3).toString());
  return results;
};

const sort = async (df, sorters = {}) => {
  // console.log('data.service > dataframes > sort > df.toString() :', df.toString());
  console.log('data.service > dataframes > sort > sorters :', sorters);
  const result = df;
  return result;
};

const paginate = async (req, results) => {
  const df = req.app.get('df');

  console.log('data.service > dataframes > paginate > df.tail(1).toString() :\n', df.tail(1).toString());
  const maxIndex = df.count() - 1; // data-forge
  console.log('data.service > dataframes > paginate > maxIndex :', maxIndex);

  const pagesLen = Math.ceil(df.count() / req.query.limit) || 1; // data-forge
  const perPage = parseInt(req.query.limit, 10) || 1;
  console.log('data.service > dataframes > paginate > pagesLen :', pagesLen);
  console.log('data.service > dataframes > paginate > perPage :', perPage);

  let res = results;

  let page = parseInt(req.query.page, 10) || 1;
  let startIdx = (page - 1) * perPage;
  const endIdx = startIdx + perPage - 1;
  let locRowIndices = [startIdx, endIdx];

  if (page > pagesLen) {
    page = pagesLen;
    startIdx = perPage * (pagesLen - 1);
    locRowIndices = [startIdx, maxIndex];
  }

  if (res.count() < req.query.limit) {
    page = 1;
  }

  console.log('data.service > dataframes > paginate > page :', page);
  console.log('data.service > dataframes > paginate > locRowIndices :', locRowIndices);

  res = res.between(locRowIndices[0], locRowIndices[1]);
  return {
    pg: page,
    pgLen: pagesLen,
    perPg: perPage,
    results: res,
  };
};

const restructureResp = async (req, results, onlyOne) => {
  const headers = req.app.get('headers');
  let res;
  if (onlyOne) {
    res = results;
  } else {
    res = results.renameSeries(headers).toArray();
  }
  return res;
};

const buildResp = async (req, options = {}) => {
  // console.log('data.service > dataframes > buildResp > df.toString() :', df.toString());
  console.log('\ndata.service > dataframes > buildResp > options :', options);
  const df = req.app.get('df');

  let pagination;
  let count;

  const totalItems = df.count(); // data-forge
  console.log('data.service > dataframes > buildResp > totalItems :', totalItems);

  const request = {
    query: req.query,
    params: req.params,
  };

  let results = df;

  const opts = {
    searchId: options.searchId || null,
    searches: options.searchers || null,
    filters: options.filters || null,
    sorters: options.sorters || null,
  };
  console.log('data.service > dataframes > buildResp > opts :', opts);

  const searchOnlyOne = opts.searchId > 0;

  if (searchOnlyOne) {
    // search by ID
    results = df.at(opts.searchId - 1);
    console.log('data.service > dataframes > buildResp > results :', results);
    pagination = {
      perPg: 1,
      pageNumber: 1,
      totalPAges: 1,
    };
    count = results ? 1 : 0;
  } else {
    // search
    results = await search(results, opts.searchers);

    // filter
    results = await filter(results, opts.filters);

    // sort
    results = await sort(results, opts.sorters);

    // paginate results
    pagination = await paginate(req, results);
    results = pagination.results;
    count = results.count();
  }
  console.log('data.service > dataframes > buildResp > results :\n', results.toString());

  // rebuild response with original headers
  results = await restructureResp(req, results, searchOnlyOne);

  const data = {
    data: results, // data-forge
    infos: {
      result: count, // data-forge
      total: totalItems,
      limit: pagination.perPg,
      pageNumber: pagination.pg,
      totalPages: pagination.pgLen,
    },
    ...request,
    schema: req.app.get('dfSchema'), // data-forge
  };
  return data;
};

module.exports = {
  len,
  search,
  filter,
  sort,
  paginate,
  restructureResp,
  buildResp,
};
