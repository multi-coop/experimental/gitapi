'use strict';
const config = require('./src/config/config');
const app = require('./src/index');

app.listen(config.port, () => console.log(`Local app - ${config.env} - listening on port : ${config.port}`));
